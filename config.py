"""
 config.py - Configuration variables for Redis FRBR datastore
"""
__author__ = "Jeremy Nelson"

REDIS_HOST = 'solr.coloradocollege.edu'
#REDIS_HOST = '0.0.0.0'
REDIS_PORT = 6379
REDIS_DB = 0
REDIS_TEST_DB = 1
REDIS_CODE4LIB_DB = 2

WEB_HOST = '0.0.0.0'
WEB_PORT = 8080
PRESENTATION_PORT = 8081

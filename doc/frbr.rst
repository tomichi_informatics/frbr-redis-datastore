:mod:`frbr` and :mod:`test_frbr` Documentation
==============================================

Overview
^^^^^^^^
The :mod:`frbr` and :mod:`test_frbr` provide a light-weight python
class wrapper and associated unit tests for basic manipulation of a 
`Redis <http://www.redis.io>`_ datastore.

`frbr` Contents
^^^^^^^^^^^^^^^
.. automodule:: frbr
   :members:

`test_frbr` Contents
^^^^^^^^^^^^^^^^^^^^
.. automodule:: test_frbr
   :members:

`features.frbr_usecases` Contents
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: frbr_usecases
   :members:
